//
//  LoginViewController.swift
//  Assignment
//
//  Created by Shubham Vinod Kamdi on 30/09/21.
//

import UIKit
import SwiftyJSON
class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginBtn.addTarget(self, action: #selector(loginService), for: .touchUpInside)
        addDoneButtonOnKeyboard(textField: usernameTextField)
        addDoneButtonOnKeyboard(textField: passwordTextField)
        passwordTextField.isSecureTextEntry = true
    }

    func addDoneButtonOnKeyboard(textField: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textField.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }

    @objc func loginService() {
        if let name = usernameTextField.text , EmailValidation.isValidEmail(emailStr: name) {
            if let password = passwordTextField.text {
                if Reachability.isInternetAvailable() {
                    let URL = URL(string: "\(Constant.SERVER_URL)\(Constant.LOGIN)")
                    if let url = URL {
                        var request = URLRequest(url: url)
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.addValue("\(Constant.STATIC_TOKEN)", forHTTPHeaderField: "Token")
                        let parameters: [String: Any] = [
                            "email": name as Any,
                            "password": password as Any,
                            "language": "en" as Any,
                            "source":"ios" as Any
                        ]
                        request.httpMethod = "post"
                        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
                        let configuration = URLSessionConfiguration.default
                        let session = URLSession(configuration: configuration)
                        let dataTask = session.dataTask(with: request){data,response,Error in
                            if Error == nil {
                                if let data = data {
                                    let Response = JSON(data)
                                    print(Response)
                                    if Response["status"].boolValue {
                                        //GOOD TO GO IN
                                        if let name = Response["user"]["first_name"].string {
                                            //Extracted Name
                                            print(name)
                                            DispatchQueue.main.async {
                                                let VC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                                VC.name = name
                                                self.present(VC, animated: true, completion: nil)
                                            }
                                        }
                                    }
                                }
                            }
                        }.resume()
                    }
                } else {
                    //INTERNET NOT AVAILABLE
                }
            } else {
                //EMPTY PASSWORD
            }
        } else {
            //EMPTY EMAIL or INVALID EMAIL
        }
    }
}
