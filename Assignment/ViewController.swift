//
//  ViewController.swift
//  Assignment
//
//  Created by Shubham Vinod Kamdi on 29/09/21.
//

import UIKit

class ViewController: UIViewController {
    var name: String = ""
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var btnClick: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnClick.setTitle("Update Color", for: .normal)
        btnClick.addTarget(self, action: #selector(computeHex) , for: .touchUpInside)
        addDoneButtonOnKeyboard(textField: nameText)
        nameText.layer.borderWidth = 1.2
        nameText.layer.borderColor = UIColor.lightGray.cgColor
        nameText.layer.cornerRadius = 17
        nameText.setLeftPaddingPoints(5)
        nameText.setRightPaddingPoints(5)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(name)
        nameText.text = name
        computeHex()
    }

    //CONVERT THE ASCII VALUES OF NAME STRING TO HEX AND USE THESE HEX VALUES AS HEX COLOR CODE
    @objc func computeHex() {
        var deci: Int = 0
        for chr in nameText.text ?? " " {
            deci += Int(chr.asciiValue ?? 0) * 1000
            var st = String(format:"%02X", deci as CVarArg)
            print(Array(st))
            if st.count != 6 {
                for _ in 0 ..< (6 - st.count) {
                    st.append("F")
                }
            }
            print(st)
            colorView.backgroundColor = hexStringToUIColor(st)
        }
    }
    
    func hexStringToUIColor (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    // done button
    func addDoneButtonOnKeyboard(textField: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textField.inputAccessoryView = doneToolbar

    }

    @objc func doneButtonAction() {
        nameText.resignFirstResponder()
    }

}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
