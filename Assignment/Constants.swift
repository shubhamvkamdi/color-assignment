//
//  Constants.swift
//  Assignment
//
//  Created by Shubham Vinod Kamdi on 30/09/21.
//

import Foundation
import UIKit
class Constant: NSObject{
    public static let STATIC_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJDSSBCb290c3RyYXAgMyIsImlhdCI6MTQ5NjQ3MjUzNiwianRpIjoiZDA4MjA4MDkwODcwZWI5MjU3NzIwNzczYWRmZDFjMWEiLCJhcGlfa2V5IjoiczU2Ynk3MzIxMjM0MzI4OWZkc2ZpdGRzZG5lIn0.3SbeLxvOO2yqIUP6IVVSq1DQmigWw1KPxprrXAC2eD8"
    public static let SERVER_URL = "http://app.osoolalkabsa.com/wsapp/home/"
    public static let LOGIN = "login"
}

class EmailValidation{
    class func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
